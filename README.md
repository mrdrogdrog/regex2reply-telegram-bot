# README #

This Telegram-bot takes a file with a Regex2Reply dict in json and a Telegram-Bot-Api-Key and replys to every matching message.

# INSTALL #

This bot uses the Telegram-Bot-API telepot from https://github.com/nickoala/telepot.
You can install it via

```
#!bash

pip3 install telepot
```


### How To Use This Bot ###

Create a regex2reply file. Its simple. Its just a json dicionary like
```
#!json
{
	"apikey": "[TELEGRAM API KEY GOES HERE]",
        "timeout": 300
	"behaviour": {
		"(Hi\\!)": [
			"Hi %USER%"
		]
	}
}

```
And then just start the bot.
Timeout is optional.

The bot supports multiple configs at the same time.
   
```
#!bash
   
python3 regexbot.py example.json more.json moremore.json moremoremore.json .......
```
or
```
#!bash
   
python3 regexbot.py *.json .......
```
