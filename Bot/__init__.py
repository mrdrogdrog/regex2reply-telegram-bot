import telepot
import random
import re
import time

class Bot(telepot.Bot):

    def __init__(self, regex2reply, apikey, timeout, debug=False):
        super(Bot, self).__init__(apikey)

        self._debug = debug

        self._me = self.getMe()
        self._lastmessage = 0
        self._timeout = timeout

        # compile every regex and put it in a new dict
        # for perfomance reason
        cregex2reply = {}
        for regex in regex2reply:
            cregex2reply[
                re.compile(regex, re.I)
            ] = regex2reply[regex]

        self._regex2reply = cregex2reply

        self.log(str(len(cregex2reply)) + " regex compiled..")

        self.message_loop()
        self.log("waiting...")

    def log(self, msg):
        print("[" + self._me["first_name"] + " (@" + self._me["username"] + ")] " + msg)

    def handle(self, msg):

        if time.time() <= self._lastmessage + self._timeout:
            return

        if "text" not in msg:  # only handle text messages...
            return

        # dump some vars...
        if "username" in msg["from"]:
            username = msg["from"]["username"]
        else:
            username = "Unkown"
        firstname = msg["from"]["first_name"]
        text = msg["text"]
        chatid = msg["chat"]["id"]


        # check if any regex is matching
        for reg in self._regex2reply:
            if not reg.search(text) is None:

                # choose one random reply and replace the USER placeholder
                reply = random.choice(self._regex2reply[reg]).replace("%USER%", firstname)

                if self._debug is True:
                    # debug
                    self.log("[debug] incoming message  " + str(msg))
                self.log("replying \"" + text + "\" from \"" + firstname + "(@" + username + ")\"  with \"" + reply + "\"")

                # send it back..
                self.sendMessage(chatid, reply)
                self._lastmessage = time.time()

                return
