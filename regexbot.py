import time
import os.path
import json
import argparse
import Bot


# check arguments and parse them
parser = argparse.ArgumentParser(description="arguments")
parser.add_argument("configfile", nargs="+")
parser.add_argument("--debug", action="store_true")
args = parser.parse_args()
print("[Core] Starting telegram regex bot.. ")

for config in args.configfile:
    # check if regex2reply file exists
    print("[Core] Loading config " + config)
    if not os.path.isfile(config):
        print(config + " not found")
        exit(1)

    # load it..
    file = open(config, 'r')
    filecontent = json.load(file)
    file.close()

    if args.debug is True:
        print("[Core] [debug] loaded content is \n" + str(filecontent) + "\n")

    regex2reply = filecontent["behaviour"]
    apikey = filecontent["apikey"]
    if "timeout" in filecontent:
        timeout = filecontent["timeout"]
    else:
        timeout = 0

    # init the bot!
    Bot.Bot(regex2reply, apikey, timeout, debug=args.debug)


# make the mainthread sleep.. a long long time (ago)
while 1:
    time.sleep(10)
